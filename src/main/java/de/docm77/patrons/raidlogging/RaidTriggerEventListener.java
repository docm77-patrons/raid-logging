package de.docm77.patrons.raidlogging;

import k3kdude.DiscordWebhook;
import org.bukkit.Raid;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.raid.*;
import org.bukkit.plugin.java.JavaPlugin;

import java.awt.*;
import java.io.IOException;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

public class RaidTriggerEventListener implements Listener {
    JavaPlugin plugin;
    String webhookUrl;

    public RaidTriggerEventListener(JavaPlugin plugin, String webhookUrl) {
        this.plugin = plugin;
        this.webhookUrl = webhookUrl;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        plugin.getLogger().info("Registered RaidTriggerEvent");
    }

    @EventHandler
    public void onRaidTrigger(RaidTriggerEvent e){
        DiscordWebhook webhook = new DiscordWebhook(webhookUrl);
        Player player = e.getPlayer();
        Raid raid = e.getRaid();
        String world = raid.getLocation().getWorld().getName();
        String location = raid.getLocation().getBlockX()+" "+raid.getLocation().getBlockY()+" "+raid.getLocation().getBlockZ();

        plugin.getLogger().info(player.getDisplayName() +"just triggered a raid at "+ location +"("+ world +")");
        webhook.addEmbed(new DiscordWebhook.EmbedObject()
                .setTitle("Raid started")
                .setColor(new Color(13632027))
                .setUrl("http://patreons.docm77.de:8123/?worldname="+world+"&mapname=flat&zoom=6&x="+raid.getLocation().getBlockX()+"&y="+raid.getLocation().getBlockY()+"&z="+raid.getLocation().getBlockZ())
                .setAuthor(player.getDisplayName(),"","https://crafatar.com/avatars/"+player.getUniqueId()+"?overlay")
                .addField(":pushpin: Coordinates", location, true)
                .addField(":globe_with_meridians: World", world, true)
//                .addField("<:Bad_Omen:636635415763484708> Raid Duration", raid.getActiveTicks()+"",true)
//                .addField(":arrows_counterclockwise: Raid Progress", raid.getSpawnedGroups()+"/"+raid.getTotalGroups(),true)
//                .addField(":loudspeaker: Raid Status", raid.getStatus().toString(),true)
            );
        try {
            webhook.execute();
        } catch (IOException ex){
            plugin.getLogger().log(Level.WARNING, "@DaniDipp Can't send webhook message", ex);
        }
    }

    @EventHandler
    public void onRaidStop(RaidStopEvent e){

        DiscordWebhook webhook = new DiscordWebhook(webhookUrl);
        Raid raid = e.getRaid();
        String world = raid.getLocation().getWorld().getName();
        String location = raid.getLocation().getBlockX()+" "+raid.getLocation().getBlockY()+" "+raid.getLocation().getBlockZ();
        Set<String> heroesSet = new HashSet<>();
        for(UUID uuid : raid.getHeroes()){ heroesSet.add(plugin.getServer().getOfflinePlayer(uuid).getName()); }
        String heroes = String.join("\n", heroesSet);

        plugin.getLogger().info("RaidStopEvent at "+ location +"("+ world +")");
        webhook.addEmbed(new DiscordWebhook.EmbedObject()
                .setTitle("Raid ended")
                .setColor(new Color(13632027))
                .setUrl("http://patreons.docm77.de:8123/?worldname="+world+"&mapname=flat&zoom=6&x="+raid.getLocation().getBlockX()+"&y="+raid.getLocation().getBlockY()+"&z="+raid.getLocation().getBlockZ())
//                .setAuthor(player.getDisplayName(),"","https://crafatar.com/avatars/"+player.getUniqueId()+"?overlay")
                .addField(":pushpin: Coordinates", location, true)
                .addField(":globe_with_meridians: World", world, true)
                .addField("<:Bad_Omen:636635415763484708> Raid Duration", raid.getActiveTicks()+"",true)
                .addField(":arrows_counterclockwise: Raid Progress", raid.getSpawnedGroups()+"/"+raid.getTotalGroups(),true)
                .addField(":loudspeaker: Raid Result", raid.getStatus().toString(),true)
                .addField(":pencil: Raid Status", e.getReason().toString(),true)
                .addField("<:Emerald:637021897753624586> Heroes",heroes,true)
            );
        try {
            webhook.execute();
        } catch (IOException ex){
            plugin.getLogger().log(Level.WARNING, "@DaniDipp Can't send webhook message", ex);
        }
    }

}
