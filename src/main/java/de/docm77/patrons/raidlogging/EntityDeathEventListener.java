package de.docm77.patrons.raidlogging;

import java.awt.Color;
import k3kdude.DiscordWebhook;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Raider;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffectType;

import java.io.IOException;
import java.util.logging.Level;

public class EntityDeathEventListener implements Listener {
    JavaPlugin plugin;
    String webhookUrl;

    public EntityDeathEventListener(JavaPlugin plugin, String webhookUrl) {
        this.plugin = plugin;
        this.webhookUrl = webhookUrl;

        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        plugin.getLogger().info("Registered EntityDeathEvent");
    }

    @EventHandler
    public void onDeath(EntityDeathEvent e){
        Entity entity = e.getEntity();
        Entity killer = e.getEntity().getKiller();
        if (killer instanceof Player && entity instanceof Raider && ((Raider) entity).isPatrolLeader()){
            DiscordWebhook webhook = new DiscordWebhook(webhookUrl);
            String player = ((Player) killer).getDisplayName();
            String effect = ((Player) killer).hasPotionEffect(PotionEffectType.BAD_OMEN) ? (((Player) killer).getPotionEffect(PotionEffectType.BAD_OMEN).getDuration() >= 120000 ? "and got Bad Omen" : "(and already has Bad Omen)") : "and doesn't have Bad Omen";
            String world = killer.getWorld().getName();
            String location = killer.getLocation().getBlockX()+" "+killer.getLocation().getBlockY()+" "+killer.getLocation().getBlockZ();

            plugin.getLogger().info(player +" just killed Raid Leader "+ effect +" at "+ location +" ("+world+")");
            webhook.addEmbed(new DiscordWebhook.EmbedObject()
                .setTitle("Raid Leader killed")
                .setColor(new Color(16085027))
                .setUrl("http://patreons.docm77.de:8123/?worldname="+world+"&mapname=flat&zoom=6&x="+killer.getLocation().getBlockX()+"&y="+killer.getLocation().getBlockY()+"&z="+killer.getLocation().getBlockZ())
                .setAuthor(player,"","https://crafatar.com/avatars/"+killer.getUniqueId()+"?overlay")
                .addField(":pushpin: Coordinates", location, true)
                .addField(":globe_with_meridians: World", world, true)
                .addField("<:Bad_Omen:636635415763484708> Bad Omen duration", (((Player) killer).hasPotionEffect(PotionEffectType.BAD_OMEN) ? ((Player) killer).getPotionEffect(PotionEffectType.BAD_OMEN).getDuration()+"" : "n.a."),true));
            try {
                webhook.execute();
            } catch (IOException ex){
                plugin.getLogger().log(Level.WARNING, "Can't send webhook message", ex);
            }
        }
    }
}
