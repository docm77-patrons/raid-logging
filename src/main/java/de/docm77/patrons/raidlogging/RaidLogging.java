package de.docm77.patrons.raidlogging;

import org.bukkit.plugin.java.JavaPlugin;

public class RaidLogging extends JavaPlugin {
    EntityDeathEventListener entityDeathEventListener;
    RaidTriggerEventListener raidTriggerEventListener;

    @Override
    public void onEnable() {
        saveDefaultConfig();
        String webhookUrl = getConfig().getString("webhook");

        entityDeathEventListener = new EntityDeathEventListener(this, webhookUrl);
        raidTriggerEventListener = new RaidTriggerEventListener(this, webhookUrl);


        getLogger().info("RaidLogging enabled!");
    }
    @Override
    public void onDisable() {
        getLogger().info("RaidLogging disabled!");
    }




}
